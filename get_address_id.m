function address_id = get_address_id( move_in_date, conception_dates )

% count the number of days between conception and birth
n_days = size(conception_dates, 1);
    
% for every subject, store the number of addresses
n_address = sum(~strcmp(move_in_date, 'None') & ~strcmp(move_in_data, ''), 2);

% dummy move in dates for boundaries
first_boa   = [1800; 1; 1; 0; 0; 0];
last_boa    = [3000; 1; 1; 0; 0; 0];
    
% store address intervals in numerical form
% boa -> begining of address
boa = zeros(6, n_address);
for j = 1:n_address
    boa(:, j) = sscanf(move_in_date{j}, '%f-%f-%f %f:%f:%f');
end
% adding fake begining and end dates in order to get intervals
boa = cat(2, first_boa, boa, last_boa);

% for every day, store at which address people were living
address_id = zeros(n_days, 1);
for j = 1:size(boa, 2)-1
    idx = date_interval(conception_dates, boa(1:3, j), boa(1:3, j+1));
    address_id(idx) = j;
end

end

