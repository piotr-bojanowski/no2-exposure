function [subject_id, move_in_date, lat, long, no2] = load_child()

fid = fopen('data/txt/MasterFile.Sheet1.txt', 'r');
format_string = ['%s %s %f %s %s %s %s ', ...
                    '%s %f %f %s %f ', ...      % first address
                    '%s %s %f %f %s %f ', ...   % second address
                    '%s %s %f %f %f ', ...  % third address
                    '%s %s %f %f'];
master_data = textscan(fid, format_string, ...
                    'Delimiter', '\t', 'HeaderLines', 1, ...
                    'TreatAsEmpty', {'None', '#N/A', 'not found'});
fclose(fid);

master_data = master_data(1:end-4);



nums = [3, 9, 10, 12, 15, 16, 18, 21, 22, 23];
for i = nums
    master_data{i} = num2cell(master_data{i});
end
master_data = cat(2, master_data{:});

% removing subject 40891
idx = cell2mat(master_data(:, 3)) == 40891;
master_data(idx, :) = [];

% storing the move in date in a table
move_in_date = master_data(:, [6, 14, 20]);

% storing the latitudes in a table
latitude = cell2mat(master_data(:, [9, 15, 21]));

% computing child adresses that have no date
no_date = strcmp(move_in_date, 'None');

% computing the child adresses that have no gps data
no_gps = isnan(latitude);

% computing the child adresses that have a faulty move in date
na_date = strcmp(master_data(:, [6, 14, 20]), '#N/A');

% computing child adresses that have gps data but no move in date
gps_but_no_date = ~no_gps & no_date;

% finding entries to remove
[x, ~] = find(gps_but_no_date | na_date);

% printing the identifiers of ignored subjects
fprintf('Subject id that were not treated:\n');
fprintf('%d ', cell2mat(master_data(x, 3)));
fprintf('\n');

master_data(x, :) = [];

% getting the latitudes and longitudes
lat     = cell2mat(master_data(:, [9, 15, 21]));
long    = cell2mat(master_data(:, [10, 16, 22]));
no2     = cell2mat(master_data(:, [12, 18, 23]));
move_in_date    = master_data(:, [6, 14, 20]);
subject_id      = cell2mat(master_data(:, 3));



end