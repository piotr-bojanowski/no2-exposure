from openpyxl import load_workbook
import os
import codecs
import csv
import xlrd

def xlsToTxt(path, newRoot):
    # loading the workbook in memory
    wb = xlrd.open_workbook(path)
    
    # getting file name without extension and directory
    fname = os.path.splitext(os.path.basename(path))[0]

    for ws in wb.sheets():
        # opening a new file
        newPath = os.path.join(newRoot, "{:s}.{:s}.txt".format(fname, ws.name))
        fid = open(newPath, 'w')
        for i in range(ws.nrows):
            j = 0
            ncells = len(ws.row(i))
            for cell in ws.row(i):
                if cell.ctype == 3:
                    date = xlrd.xldate_as_tuple(cell.value, wb.datemode)
                    fid.write("{:d}-{:d}-{:d} 00:00:00".format(date[0], date[1], date[2]))
                else:
                    fid.write(str(cell.value))
                if j < ncells - 1:
                    fid.write('\t')
                j = j + 1
            fid.write('\n')
        fid.close()
            

def xlsxToTxt(path, newRoot):
    # loading the workbook in memory
    wb = load_workbook(path, data_only = 2)
    
    # getting file name without extension and directory
    fname = os.path.splitext(os.path.basename(path))[0]
    
    # looping over the sheets in the file
    for ws in wb:
        # opening a new file
        newPath = os.path.join(newRoot, "{:s}.{:s}.txt".format(fname, ws.title))
        fid = open(newPath, 'w')
        for row in ws.rows:
            j = 0
            ncells = len(row)
            for cell in row:
                if type(cell.value) is unicode:
                    fid.write(cell.value.encode('utf-8'))
                else:
                    fid.write(str(cell.value))
                if j < ncells - 1:
                    fid.write('\t')
                j = j + 1
            fid.write('\n')
        fid.close()


def csvToTxt(path, newRoot):
    # opening text file
    fname = os.path.splitext(os.path.basename(path))[0]
    newPath = os.path.join(newRoot, "{:s}.txt".format(fname))
    fid = open(newPath, 'w')

    #opening csv file for reading
    with open(path, 'rb') as csvfile:
        spamreader = csv.reader(csvfile, delimiter=',')
        for row in spamreader:
            fid.write('\t'.join(row))
            fid.write('\n')
    # closing text file
    fid.close()



xlsxToTxt('data/original/MasterFile.xlsx', 'data/txt')
xlsxToTxt('data/original/CHILD_ResidentialHistories.xlsx', 'data/txt')
csvToTxt('data/original/naps2008-2012.csv', 'data/txt')
xlsToTxt('data/original/allcitiesnaps2013.xls', 'data/txt')
