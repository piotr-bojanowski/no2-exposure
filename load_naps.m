function [res, calendar] = load_naps()

fid = fopen('data/txt/naps2008-2012.txt', 'r');
naps1 = textscan(fid, '%s %s %s %f %f %f %f %f %f %f %f %f %f', ...
                    'Delimiter', '\t', 'HeaderLines', 1);
fclose(fid);

fid = fopen('data/txt/allcitiesnaps2013.Sheet1.txt', 'r');
naps2 = textscan(fid, '%s %s %f %f %f %f %f %f %f %f %f %f %f %f %f %f', ...
                    'Delimiter', '\t', 'HeaderLines', 1);
fclose(fid);

% storing all year in the same format
res = {[naps1{1}; naps2{1}], ...    % 1) center name
    [naps1{4}; naps2{12}], ...      % 2) day
    [naps1{6}; naps2{10}], ...      % 3) month
    [naps1{7}; naps2{9}], ...       % 4) year
    [naps1{5}; naps2{13}], ...      % 5) week
    [naps1{8}; naps2{4}], ...       % 6) daily average
    [naps1{9}; naps2{7}], ...       % 7) half month average
    [naps1{10}; naps2{8}]};         % 8) month average

% correction for Edmenton 2011
% finding the corresponding rows
idx = strcmp(res{1}, 'Edmonton') & res{4}==2011;
% multiplying the daily average by 100
res{6}(idx) = res{6}(idx) * 1000;

% storing all days in the study years
calendar = unique(cell2mat(res(:, (4:-1:2))), 'rows');

end